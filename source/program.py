def __init__():
	pass

def print_error():
	"""
	В случае ошибки выводит соответсвующее сообщение ('Failed! Try again!')

	Args:
		Нет аргументов

	Returns:
		None

	Raises:
		None.


	Examples:
		>>> print_error()
		'Failed! Try again!'
	"""
	print('Failed! Try again!')

def try_again():
	"""
	Выводит сообщение об ошибке и начинает выполнение программы заново

	Args:
		Нет аргументов

	Returns:
		None

	Raises:
		ValueError.


	Examples:
		>>> try_again()

	"""
	print_error()
	main()

def is_correct(n):
	"""
	Проверяет корректность введенного числа (количества чисел в последовательности)

	Args:
		n:	число для проверки

	Returns:
		None

	Raises:
		ValueError.


	Examples:
		>>> is_correct(3)
		>>> is_correct(0)
		Traceback (most recent call last):
			...
		ValueError
		>>> is_correct(-5)
		Traceback (most recent call last):
			...
		ValueError
	"""
	if n <= 0:
		raise ValueError

def test_is_correct():
	"""
	Тестирует функцию is_correct(n)

	Args:
		Нет аргументов

	Returns:
		None

	Raises:
		ValueError, AssertionError.


	Examples:
		>>> test_is_correct()

	"""
	assert is_correct(3) is None
	try:
		assert is_correct(-5) == ValueError
		assert is_correct(0) == ValueError
	except ValueError:
		pass

def test_task():
	"""
	Тестирует функцию task()

	Args:
		Нет аргументов

	Returns:
		None

	Raises:
		AssertionError.


	Examples:
		>>> test_task()

	"""
	assert task([5, 4, 2, 7, 6, 7, 2]) == '2->4->7'
	assert task([4, 3, 2, 1]) == 'Nope'

def task(numbers):
	"""
	Выполняет задание в заданной последовательности чисел находит цикл A->B->C (A связана с B, B связано с C, C связано с A). Если такой имеется, то выводит первый встретившийся. В ином случае: 'Nope'

	Args:
		numbers:	последовательность чисел

	Returns:
		str(a) + '->' + str(b) + '->' + str(c), если числа a, b и c соответствуют условию выше;
		'Nope', если в последовательности нет таких чисел

	Raises:
		Не генерирует исключений.


	Examples:
		>>> task([5, 4, 2, 7, 6, 7, 2])
		'2->4->7'
		>>> task([4, 3, 2, 1])
		'Nope'
	"""
	a, b, c = 0, 0, 0
	for e in range(len(numbers)):
		if e + 1 == numbers[e]:
			continue
		a = numbers[e - 1]
		b = numbers[a - 1]
		c = numbers[b - 1]
		if c == e:
			return str(a) + '->' + str(b) + '->' + str(c)
	return 'Nope'

def main():
	"""
	Главная функция, которая управляет ходом выполнения программы

	Args:
		Нет аргументов

	Returns:
		None

	Raises:
		ValueError.


	Examples:
		>>> main()
		7
		'5 4 2 7 6 7 2'
		'2->4->7'
		>>> main()
		4
		'4 3 2 1'
		'Nope'
		>>> main()
		3
		'5 8 2 1 3 5 4 0'
		'Failed! Try again!'
	"""
	n = 0
	numbers = []
	try:
		n = int(input())
		is_correct(n)
	except ValueError:
		try_again()

	try:
		numbers = [e for e in map(int, input().split())]
	except ValueError:
		try_again()
	if len(numbers) != n:
		try_again()
	for e in numbers:
		if e < 1:
			try_again()
		if e > n:
			try_again()

	print(task(numbers))

if __name__ == '__main__':
	main()